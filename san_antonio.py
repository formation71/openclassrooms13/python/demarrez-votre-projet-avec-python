# -*- coding: utf8 -*-
import random
import json


def read_value_from_json(file, key):
    values = []

    with open("json/" + file) as f:
        data = json.load(f)

        for entry in data:
            values.append(entry[key])
        return values


def get_random_character():
    characters = read_value_from_json('characters.json', 'character')
    return get_random_item(characters)


def get_random_quotes():
    quotes = read_value_from_json('quotes.json', 'quote')
    return get_random_item(quotes)


def get_citation(character, quote):
    return "{} a dit : {}".format(
        capitalize(character),
        capitalize(quote)
    )


def capitalize(word):
    return word.capitalize()


def get_random_item(items):
    random_number = random.randint(0, len(items) - 1)
    item = items[random_number]
    return item


def main_loop():
    user_answer = ""

    while user_answer != "B":
        print(
            get_citation(
                get_random_character(),
                get_random_quotes()
            )
        )
        user_answer = input("Taper entrée pour connaître une autre citation ou B pour quitter le programme.")


if __name__ == '__main__':
    main_loop()
