variable = "ma variable"
array = ['ma', 'liste']
array_immutable = ("mon", "tuple")  # Tuple
dictionnaire = {"mon": "dico"}

variable.capitalize()
variable.strip()
"{}".format("test")

var = (0).is_integer  # True

array.append("valeur_a_ajouter")
array.insert(0, "valeur_a_ajouter_index_0")
array.pop()
array.remove('liste')
